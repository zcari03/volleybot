import discord
from discord.ext import commands

from user import User
import volleybot


def getToken():
    with open('../TOKEN.txt') as file:
        return file.readlines()[0]

def main():
    User.setup()
    User.lookupTableFile = '../user-lookup.csv'
    User.signedInFile = '../signed-in.csv'
    User.maxRequests = 5
    User.setup()

    TOKEN = getToken()

    intents = discord.Intents.default()
    client = commands.Bot(command_prefix="!", intents=intents)
    volleybot.setup(client)
    client.run(TOKEN)

if __name__ == "__main__":
    main()