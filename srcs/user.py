import csv

class User:
    userList = []
    lookup = []
    teamList = []
    maxRequests = 5
    lookupTableFile = '../user-lookup.csv'
    signedInFile = '../signed-in.csv'
    teamFormationFile = '../teams.csv'
    
    def __init__(self, discordName, signedIn=False):
        self.discordName = discordName
        self.name, self.rank = self.lookupUser()
        self.numRequests = 0

        if signedIn:
            self.signIn()
        else:
            self.signedIn = False

    def lookupUser(self):
        for row in User.lookup: 
            if(self.discordName == row[0]):
                return row[1], row[2]

        # If we made it to here, then the user isn't in the lookup table
        tList = [self.discordName, "UNKNOWN", -1]
        User.lookup.append(tList)
        User.saveLookupTable()
        return "UNKNOWN", -1

    def signIn(self):
        self.signedIn = True
        tList = [self.name, self.rank]
        
        with open(User.signedInFile, 'a') as file:
            csvFile = csv.writer(file)
            csvFile.writerow(tList)
        return
    
    '''
        Used to print debug information
    '''
    def print(self):
        print("\tUSER:    ", self.discordName)
        print("\tNAME:    ", self.name)
        print("\tNUM_REQ: ", self.numRequests)
        print("\tRANK:    ", self.rank)
        if self.signedIn:
            print("\tSTATUS:   Signed In")
        else:
            print("\tSTATUS:   Not Signed In")



    ################################
    # Singleton Functions
    ################################

    def setup():
        with open(User.signedInFile, 'w') as file:
            csvFile = csv.writer(file)
            csvFile.writerow(['Name', 'Rank'])

    '''
        If the user is already in the system, a 0 will be returned. If not, then a 1 will be returned
    '''
    def addUser(user):
        # Check to make sure user isn't already in the system
        for tUser in User.userList:
            if user.discordName == tUser.discordName:
                return 0

        User.userList.append(user)
        return 1

    def loadLookupTable():
        with open(User.lookupTableFile, mode='r') as file:
            csvFile = csv.reader(file)

            for lines in csvFile:
                User.lookup.append(lines)
        return

    def saveLookupTable():
        with open(User.lookupTableFile, 'w') as file:
            csvFile = csv.writer(file)
            csvFile.writerows(User.lookup)
        return

    def loadTeams():
        with open(User.teamFormationFile, 'r') as file:
            csvFile = csv.reader(file)

            for lines in csvFile:
                User.teamList.append(lines)
        return

    '''
        Function will return 1 if the user can still request music, otherwise it will return 0
    '''
    def incNumRequests(discordName):
        for user in User.userList:
            if user.discordName == discordName:
                if user.numRequests < User.maxRequests:
                    user.numRequests += 1
                    return 1
            else:
                return 0

        # If we are here then the user hasn't been added to the userList
        User.addUser(User(discordName))
        return 1

    def displayUsers():
        i = 0
        for user in User.userList:
            print("USER INDEX: ", i)
            user.print()
            i += 1
