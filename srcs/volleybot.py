import random
import asyncio
import discord
from discord.ext import commands
from async_timeout import timeout
import itertools

from user import User
from musicController import VoiceConnectionError
from musicController import InvalidVoiceChannel
from musicController import YTDLSource
from musicController import MusicPlayer
from musicController import ytdlopts
from musicController import ffmpegopts
from musicController import ytdl

class Volleybot(commands.Cog):
    
    __slots__ = ('bot', 'players')

    def __init__(self, bot):
        self.bot = bot
        self.players = {}

    def get_player(self, ctx):
        """Retrieve the guild player, or generate one."""
        try:
            player = self.players[ctx.guild.id]
        except KeyError:
            player = MusicPlayer(ctx)
            self.players[ctx.guild.id] = player

        return player

    async def cleanup(self, guild):
        try:
            await guild.voice_client.disconnect()
        except AttributeError:
            pass

        try:
            del self.players[guild.id]
        except KeyError:
            pass

    async def __local_check(self, ctx):
        """A local check which applies to all commands in this cog."""
        if not ctx.guild:
            raise commands.NoPrivateMessage
        return True

    async def __error(self, ctx, error):
        """A local error handler for all errors arising from commands in this cog."""
        if isinstance(error, commands.NoPrivateMessage):
            try:
                return await ctx.send('This command can not be used in Private Messages.')
            except discord.HTTPException:
                pass
        elif isinstance(error, InvalidVoiceChannel):
            await ctx.send('Error connecting to Voice Channel. '
                           'Please make sure you are in a valid channel or provide me with one')

        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name='Join', aliases=['join', 'j'], brief="Signs you into the volleyball system")
    async def join_(self, ctx):
        user = User(ctx.author.display_name, True)
        alreadySignedIn = not User.addUser(user)

        embed = None
        if alreadySignedIn:
            embed = discord.Embed(title=f"Sorry {ctx.author.display_name}, but you already signed in!", color=discord.Color.red())
        else: 
            embed = discord.Embed(title=f"{ctx.author.display_name} has successfully signed in!", color=discord.Color.green())

        return await ctx.send(embed=embed)

    @commands.command(name='ListPlayers', aliases=['list', 'l', 'listplayers'], brief="Lists players who have signed in")
    async def listplayers_(self, ctx):
        nameList = "The following people have joined volleyball\n\n"
        for user in User.userList:
            if user.signedIn:
                nameList += user.discordName + '\n'

        embed = discord.Embed(title=f"{nameList}", color=discord.Color.blurple())

        return await ctx.send(embed=embed)

    @commands.command(name='Teams', aliases=['teams', 't'], brief="Displays teams")
    async def showteams_(self, ctx):
        User.loadTeams()
        numTeams = len(User.teamList[0])
        numPlayers = len(User.teamList) - 1 # num players per team

        embed = discord.Embed(title=f"Here are the teams", color=discord.Color.orange())

        for i in range(numTeams):
            teamList = ''
            for j in range(numPlayers):
                try:
                    # If there is a player, add them to the list
                    teamList += '\t\t' + User.teamList[j+1][i] + '\n'
                except:
                    # This catches the exception where if the teams are uneven
                    None

            embed.add_field(name='TEAM ' + str(i+1) + '\n', value=teamList, inline=False)        

        return await ctx.send(embed=embed)

    @commands.command(name='Play', aliases=['play', 'p'], brief="Request song and put it in the queue")
    async def listplayers_(self, ctx, *, search:str):
        """Request a song and add it to the queue.
        This command attempts to join a valid voice channel if the bot is not already in one.
        Uses YTDL to automatically search and retrieve a song.
        Parameters
        ------------
        search: str [Required]
            The song to search and retrieve using YTDL. This could be a simple search, an ID or URL.
        """
        if not User.incNumRequests(ctx.author.display_name):
            message = "Sorry " +  ctx.author.display_name + ", your max requests have been reached. (Max: " + str(User.maxRequests) + ")"
            embed = discord.Embed(title=message, color=discord.Color.red())
            return await ctx.send(embed=embed)

        await ctx.trigger_typing()

        vc = ctx.voice_client
        if not vc:
            await ctx.invoke(self.connect_)
        player = self.get_player(ctx)
        # If download is False, source will be a dict which will be used later to regather the stream.
        # If download is True, source will be a discord.FFmpegPCMAudio with a VolumeTransformer.
        await YTDLSource.create_source(ctx, search, loop=self.bot.loop, download=False, player=player)

    @commands.command(name='Pause', aliases=['pause'], description="Pauses music")
    async def pause_(self, ctx):
        """Pause the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_playing():
            embed = discord.Embed(title="", description="I am currently not playing anything",
                                  color=discord.Color.red())
            return await ctx.send(embed=embed)
        elif vc.is_paused():
            return

        vc.pause()
        await ctx.send("Paused ⏸️")

    @commands.command(name='Resume', aliases=['resume'], description="Resumes music")
    async def resume_(self, ctx):
        """Resume the currently paused song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            embed = discord.Embed(title="", description="I'm not connected to a voice channel",
                                  color=discord.Color.green())
            return await ctx.send(embed=embed)
        elif not vc.is_paused():
            return

        vc.resume()
        await ctx.send("Resuming ⏯️")

    @commands.command(name='Queue', aliases=['q', 'playlist', 'que', 'queue'], description="Shows list of queued songs")
    async def queue_info(self, ctx):
        """Retrieve a basic queue of upcoming songs."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            embed = discord.Embed(title="", description="I'm not connected to a voice channel",
                                  color=discord.Color.green())
            return await ctx.send(embed=embed)

        player = self.get_player(ctx)
        if player.queue.empty():
            embed = discord.Embed(title="", description="queue is empty", color=discord.Color.green())
            return await ctx.send(embed=embed)

        seconds = vc.source.duration % (24 * 3600)
        hour = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        if hour > 0:
            duration = "%dh %02dm %02ds" % (hour, minutes, seconds)
        else:
            duration = "%02dm %02ds" % (minutes, seconds)

        # Grabs the songs in the queue...
        upcoming = list(itertools.islice(player.queue._queue, 0, int(len(player.queue._queue))))
        fmt = '\n'.join(
            f"`{(upcoming.index(_)) + 1}.` [{_['title']}]({_['webpage_url']}) | ` Requested by: {_['requester']}`\n"
            for _ in upcoming)
        fmt = f"\n__Now Playing__:\n[{vc.source.title}]({vc.source.web_url}) | ` {duration} Requested by: {vc.source.requester}`\n\n__Up Next:__\n" + fmt + f"\n**{len(upcoming)} songs in queue**"
        embed = discord.Embed(title=f'Queue for {ctx.guild.name}', description=fmt, color=discord.Color.green())
        embed.set_footer(text=f"{ctx.author.display_name}", icon_url=ctx.author.avatar_url)

        await ctx.send(embed=embed)

    @commands.command(name='PowerOn', aliases=['poweron', 'pon', 'connect'], description="Turns on the music player (automatically done in the play command")
    async def connect_(self, ctx, *, channel: discord.VoiceChannel = None):
        """Connect to voice.
        Parameters
        ------------
        channel: discord.VoiceChannel [Optional]
            The channel to connect to. If a channel is not specified, an attempt to join the voice channel you are in
            will be made.
        This command also handles moving the bot to different channels.
        """
        for c in ctx.guild.channels:
            if c.type == discord.ChannelType.voice:
                if c.name == 'what\'s up?':
                    channel = c
                    print(c.name)
        
        if not channel:
            try:
                channel = ctx.author.voice.channel
            except AttributeError:
                embed = discord.Embed(title="",
                                      description="No channel to join. Please call `,join` from a voice channel.",
                                      color=discord.Color.green())
                await ctx.send(embed=embed)
                raise InvalidVoiceChannel('No channel to join. Please either specify a valid channel or join one.')

        vc = ctx.voice_client

        if vc:
            if vc.channel.id == channel.id:
                return
            try:
                await vc.move_to(channel)
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'Moving to channel: <{channel}> timed out.')
        else:
            try:
                await channel.connect()
                # await asyncio.wait_for(channel.connect(), timeout=30, loop=self.loop)
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'Connecting to channel: <{channel}> timed out.')
        if (random.randint(0, 1) == 0):
            await ctx.message.add_reaction('👍')
        await ctx.send(f'**Joined `{channel}`**')

    @commands.command(name='PowerOff', aliases=["poweroff", "disconnect", "poff"],
                      description="stops music and disconnects from voice")
    async def leave_(self, ctx):
        """Stop the currently playing song and destroy the player.
        !Warning!
            This will destroy the player assigned to your guild, also deleting any queued songs and settings.
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            embed = discord.Embed(title="", description="I'm not connected to a voice channel",
                                  color=discord.Color.green())
            return await ctx.send(embed=embed)

        if (random.randint(0, 1) == 0):
            await ctx.message.add_reaction('👋')
        await ctx.send('**Successfully disconnected**')

        await self.cleanup(ctx.guild)

def setup(bot):
    bot.add_cog(Volleybot(bot))