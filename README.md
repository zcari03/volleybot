# Volleybot
Discord bot to handle volleyball nights

# How to use

## Volleyball Specific Commands
| Command | Description | Aliases |
| :-----: | :---------: | :-----: |
| `!help` | Displays commands that can be displayed | None |
| `!Join` | Signs you into the system | `!join`, `!j` |
| `!ListPlayers` | Lists what players have signed in using their discord names | `!listplayers`, `!l` |
| `!Teams` | Shows the teams that have been generated | `!teams`, `!t` |

## Music Commands
| Command | Description | Aliases |
| :-----: | :---------: | :-----: |
| `!Play <song_name>` | Queues a song that the bot will play | `!play`, `!p` |
| `!Queue` | Displays the current queue | `!queue`, `!q` |
| `!Pause` | Pauses music being played | `!pause` |
| `!Resume` | Resumes paused music | `!resume` |

# Dependancies 
```
pip install discord
pip install youtube_dl
pip install PyNaCl
```

# File Formats
The only file that is name dependant is `TOKEN.txt`. Otherwise, the filenames can be set in `main()`

## TOKEN
This is a .txt file that contains the token for the Discord Bot. The first line in the file should contain the token

## User Lookup Table
This file is used as a lookup table to translate a user's discord name into their real name. It is also used to get the user's rank information.

| Discord Name | Real Name | Rank |
| :----------: | :-------: | :--: |
| [Player Discord Name] | [Player Real Name] | [Player's Skill Level]

## Signed In List
The list is a .csv file and arrranged like so:

| Name (String) | Rank (Range 1-10) |
| :-----------: | :---------------: |
| [Plasyer's Real Name] | [Player's Skill Level] |

This file is only written to and not read from. It is also erased everytime the bot starts

## Teams List
The list is a .csv file and contains information on how the teams are divided. It is only read by the program and not written to (as another program arranges everyone into teams). The format must be as follows:

- n = total number of teams
- m = total number of players

| [Team 1 Name] | [Team 2 Name] | ... | [Team n Name] |
| :-----------: | :-----------: | :-: | :-----------: |
| [Player 1 Name] | [Player 2 Name] | ... | [Player n Name] |
| ... | ... | ... | ... |
| ... | ... | ... | [ Player m Name] |

The first line of the file is only used to determine the number of teams there are. Otherwise, it is ignored.The program can also handle teams that aren't even.